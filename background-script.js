/*
 * Helpers to be injected into page frames to get selected text
 */
function getSelectionText() {
  let text = "";
  if (window.getSelection) {
      let selection = window.getSelection();
      if (selection) {
        text = selection.toString();
      }
  } else if (document.selection && document.selection.type != "Control") {
      text = document.selection.createRange().text;
  }
  return text;
}
const getSelectionTextInjectable = `( ${getSelectionText.toString()} )()`

/*
 * Main functions
 */
const DEFAULT_SEARCH_URL = "https://dictionary.cambridge.org/search/direct/?datasetsearch=english&q="

function openDictionaryPage(term) {
  if (!term) {
    return
  }

  browser.storage.sync.get("searchUrl").then(
    (result) => {
      let searchUrl = result.searchUrl || DEFAULT_SEARCH_URL
      browser.windows.create({
        url: [searchUrl + encodeURI(term)]
      }).then(() => {}, () => {});
    },
    () => console.log(`Error: ${error}`)
  )
};

function openDictionaryWithSelection() {
  browser.tabs.executeScript({
    code: getSelectionTextInjectable,
    allFrames: true
  }).then(
    results => results && openDictionaryPage(results.join('')),
    error => console.log(`Error: ${error}`)
  )
};

/*
 * Extension interfaces to end-user: 
 * - context-menu
 * - extention button in toolbar
 * - key-binding
 */
browser.contextMenus.create({
  id: "search-dictionary",
  title: 'Search in dictionary',
  contexts: ["selection"]
}, () => {});
browser.contextMenus.onClicked.addListener(function(info, tab) {
  if (info.menuItemId === "search-dictionary") {
      openDictionaryPage(info.selectionText);
  }
});
browser.browserAction.onClicked.addListener(openDictionaryWithSelection);
browser.commands.onCommand.addListener(command => {
  if (command === 'search-selection') {
    openDictionaryWithSelection();
  }
});

