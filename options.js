function saveOptions(e) {
    e.preventDefault();
    browser.storage.sync.set({
        searchUrl: document.querySelector("#searchUrl").value
    });
}

function restoreOptions() {

    function setCurrentChoice(result) {
        document.querySelector("#searchUrl").value = result.searchUrl;
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    var getting = browser.storage.sync.get("searchUrl");
    getting.then(setCurrentChoice, onError);
}
  
document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);